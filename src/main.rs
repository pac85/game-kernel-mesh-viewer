extern crate game_kernel;
extern crate cgmath;
#[macro_use]
extern crate imgui;
extern crate gltf;

use cgmath::*;
use game_kernel::game_kernel::ecs::{AddEntity, SystemKey, World, EntitId};
use game_kernel::game_kernel::subsystems::Subsystems;
use game_kernel::renderer::{
    model::Model,
    mesh::*,
    texture::*,
    material::Material,
    light::*,
};

use std::sync::Arc;
use std::cell::RefCell;
use std::convert::TryFrom;

//this should really be part of the engine perhaps
fn init_systems(sbsts: &mut Subsystems) -> (SystemKey, SystemKey, SystemKey, SystemKey) {
    let mut renderer = sbsts.renderer.renderer.as_ref().unwrap().clone();

    let renderer_system = Arc::new(RefCell::new(
        game_kernel::game_kernel::core_systems::renderer::RendererSystem::new(renderer.clone()),
    ));
    let static_mesh_system = Arc::new(RefCell::new(
        game_kernel::game_kernel::core_systems::static_mesh::StaticMeshSystem::new(),
    ));
    let light_system = Arc::new(RefCell::new(
        game_kernel::game_kernel::core_systems::light::LightSystem::new(),
    ));
    let camera_system = Arc::new(RefCell::new(game_kernel::CameraSystem::new()));

    let rs = sbsts
        .systems_manager
        .add_system(renderer_system)
        .expect("rs");
    let sk = sbsts
        .systems_manager
        .add_system(static_mesh_system)
        .expect("sk");
    let lk = sbsts.systems_manager.add_system(light_system).expect("lk");
    let ck = sbsts.systems_manager.add_system(camera_system).expect("ck");

    sbsts.systems_manager
        .init_system(&rs, sbsts.world.borrow().deref())
        .unwrap();

    (rs, sk, lk, ck)
}

use game_kernel::input::Input;
use std::ops::Deref;

fn setup_input(sbsts: &mut Subsystems) {
    use game_kernel::input::KeyTypes;
    sbsts.input.bind_action(KeyTypes::KeyBoard(17), "forward");
    sbsts.input.bind_action(KeyTypes::KeyBoard(31), "backward");
    sbsts.input.bind_control("forward", "forward", 1.0);
    sbsts.input.bind_control("backward", "forward", -1.0);

    sbsts.input.bind_action(KeyTypes::KeyBoard(32), "left");
    sbsts.input.bind_action(KeyTypes::KeyBoard(30), "right");
    sbsts.input.bind_control("left", "side", 1.0);
    sbsts.input.bind_control("right", "side", -1.0);
}

fn flycam<W: Deref<Target = World>>(world: W, camera_entity_index: EntitId, delta_time:  f32, r: Vector2<f32>, input: &Input) {
    let components_mutex = world.get_entity_components(camera_entity_index).unwrap();
    let components = components_mutex;
    let mut camerac = components.iter().next().unwrap().borrow_mut();
    let camera = camerac
        .gk_as_any_mut()
        .downcast_mut::<game_kernel::CameraComponent>()
        .unwrap();

    camera.camera.dir =
        cgmath::Vector3::new(r.x.sin() * r.y.cos(), -r.y.sin(), r.x.cos() * r.y.cos());

    use cgmath::prelude::InnerSpace;
    let velocity = if input.is_action_down("sprint") {
        0.04
    } else {
        0.02
    };

    camera.camera.eye +=
        camera.camera.dir * delta_time * velocity * input.get_control_value("forward").unwrap();
    camera.camera.eye += camera.camera.dir.cross(camera.camera.up).normalize()
        * delta_time
        * velocity
        * input.get_control_value(&"side".to_owned()).unwrap();
}

use game_kernel::game_kernel::subsystems::video::common::vulkan::VulkanBasic;

use std::io::BufReader;
use std::fs::File;

fn load_model(sbsts: &Subsystems ) -> (Skeleton, SkeletalMesh, Model, SkeletalAnimation) {
    let (document, buffers, images) = gltf::import("models/guy.glb").unwrap();
    let mut nodes = document.default_scene().unwrap().nodes();

    let mesh_node = nodes
        .next()
        .unwrap()
        .children()
        .find(|node| node.mesh().is_some())
        .unwrap();

    let mut nodes = document.default_scene().unwrap().nodes();
    let armature_node = nodes
        .find(|node| node.name() == Some("Armature"))
        .unwrap();

    let mut skeletal_data = SkeletalMeshData::try_from((
        mesh_node,
        armature_node,
        &buffers[0].clone(),
    )).unwrap();

    let skeleton = skeletal_data.skeleton.clone();
    //later used for animations
    let tskeleton = skeletal_data.skeleton.clone();
    skeletal_data.skeleton = skeleton.skeleton_frame();

    let skeletal_mesh = SkeletalMesh::from_skeleton_data(
        sbsts.get_vulkan_common().unwrap().get_transfers_queue(),
        &skeletal_data,
    ).unwrap();

    let mesh = Mesh::SkeletalMesh(skeletal_mesh.clone());

    let renderer = sbsts.renderer.renderer.as_ref().unwrap().clone();

    let treader1 = BufReader::new(File::open("textures/floor.ktx").unwrap());
    let test_texture1 = game_kernel::game_kernel::subsystems::video::renderer::texture::Texture::from_texture_data(
        game_kernel::game_kernel::subsystems::video::renderer::texture::TextureData::load_from_kxt(treader1).unwrap(),
        game_kernel::game_kernel::subsystems::video::renderer::texture::TextureUsage::Color,
        sbsts.get_vulkan_common().unwrap(),
        Some(RGB8LINEAR),
    ).unwrap();
    let treadern = BufReader::new(File::open("textures/flat_normals.ktx").unwrap());
    let test_texturen = game_kernel::game_kernel::subsystems::video::renderer::texture::Texture::from_texture_data(
        game_kernel::game_kernel::subsystems::video::renderer::texture::TextureData::load_from_kxt(treadern).unwrap(),
        game_kernel::game_kernel::subsystems::video::renderer::texture::TextureUsage::Normal,
        sbsts.get_vulkan_common().unwrap(),
        Some(RGB8LINEAR),
    ).unwrap();

    let material = unsafe{Material::test_new(sbsts.get_vulkan_common().unwrap().get_device(), Arc::new(test_texture1), Arc::new(test_texturen))};


    let gltf_animation = document.animations().next().unwrap();
    let anim = SkeletalAnimation::try_from((gltf_animation, &tskeleton, &buffers[0])).unwrap();

    (
        tskeleton,
        skeletal_mesh,
        Model{material: Arc::new(material), mesh: Arc::new(mesh), transform: cgmath::Matrix4::from_translation(cgmath::Vector3::new(0.0, 5.0, 0.0))},
        anim,
    )
}

fn main() {
    let mut sbsts = game_kernel::game_kernel::subsystems::init().unwrap();

    let (
            render_system_key, 
            _, 
            light_system_key, 
            camera_system_key
        ) = init_systems(&mut sbsts);

    let (skeleton, mut skeletal_mesh, model, mut anim) = load_model(&sbsts);

    let renderer = sbsts.renderer.renderer.as_ref().unwrap().clone();
    renderer.borrow_mut().add_model(model);

    //needs at least one light to see anything
    let mut light = Light {
        light: LightType::Directional(DirectionalLight {
            color: cgmath::Vector3::new(20f32, 9f32, 5f32),
            direction: cgmath::Vector3::new(0.2f32, -1.0f32, -1.3f32) / 1.4142,
            angle: 0.1,
            n_cascades: 3,
            cascade_scale: 6f32,
        }),
        shadow_map_size: Some(2048),
    };

    renderer.borrow_mut().add_light(light);
    //-----------------------------------------

    //sets up camera
    let camera = game_kernel::Camera::new(
        Point3::new(0.0, 0.0, -6.0),
        Vector3::new(1.0, 0.0, 0.0),
        Vector3::new(0.0, -1.0, 0.0),
        Rad::turn_div_4() / 1f32,
        (0.1, 1000.0),
    );

    let camera_entity =
        AddEntity::new(sbsts.world.borrow_mut(), None)
            .unwrap()
            .with_component(game_kernel::CameraComponent::new(camera))
            .entity;
    let camera_id = sbsts
        .systems_manager
        .get_system_by_type::<game_kernel::CameraSystem>()
        .unwrap();
    sbsts
        .systems_manager
        .get_system(camera_id)
        .unwrap()
        .borrow_mut()
        .downcast_mut::<game_kernel::CameraSystem>()
        .unwrap()
        .set_active_camera(camera_entity);


    let mut camera_rotation = Vector2::new(0.0f32, 0.0f32);
    //---------------------------------------------------

    setup_input(&mut sbsts);

    let (mut show_entities, mut show_perfromance) = (false, false);
    let mut elapsed = 0f32;
    let queue = sbsts.get_vulkan_common().unwrap().get_graphics_queue();
    sbsts.main_loop(move |delta, mouse_pos, buttons, world, ui, input| {
        camera_rotation += delta * mouse_pos / 10000f32;
        camera_rotation.y = camera_rotation.y.min(3.1415 / 2.0).max(-3.1415 / 2.0);

        flycam(world, camera_entity, delta, camera_rotation, input);

        //updates skeleton for animation
        let sframe = anim.get_key_frame(elapsed*0.01).skeleton_frame();
        elapsed += delta;
        elapsed %= 1041.70;

        unsafe{
            skeletal_mesh.update_skeleton(&sframe, queue.clone());
        }
        //-----------------------------
        
        //imgui
        let mut should_run = true;
        let mmb = ui.begin_main_menu_bar().unwrap();
        ui.menu(im_str!("file"), true, || {
            if ui.small_button(im_str!("quit")) {
                should_run = false;
            }
        });
        ui.menu(im_str!("windows"), true, || {
            ui.checkbox(im_str!("performance"), &mut show_perfromance);
            ui.checkbox(im_str!("entities inspector"), &mut show_entities);
        });
        mmb.end(&ui);

        //if false the engine exits the main loop
        should_run
    });

}
